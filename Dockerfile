FROM openjdk:17-jdk
COPY . /ws
WORKDIR /ws
ENTRYPOINT ["./mvnw", "spring-boot:run", "-f", "pom.xml"]
