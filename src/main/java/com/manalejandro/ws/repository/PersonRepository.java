package com.manalejandro.ws.repository;

import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.manalejandro.ws.model.Person;

import jakarta.annotation.PostConstruct;

@Component
public class PersonRepository {
	private static final Map<String, Person> persons = new HashMap<String, Person>();

	@PostConstruct
	public void initData() throws DatatypeConfigurationException {
		Person person1 = new Person();
		person1.setName("name1");
		person1.setTelephone(600123123);
		person1.setAddress("City 1");
		person1.setBirthday(DatatypeFactory.newInstance().newXMLGregorianCalendar(new GregorianCalendar(1980,0,1)));

		persons.put(person1.getName(), person1);

		Person person2 = new Person();
		person2.setName("name2");
		person2.setTelephone(600123123);
		person2.setAddress("City 2");
		person2.setBirthday(DatatypeFactory.newInstance().newXMLGregorianCalendar(new GregorianCalendar(1980,0,1)));

		persons.put(person2.getName(), person2);
	}

	public Person findPerson(String name) {
		Assert.notNull(name, "The Person's name must not be null");
		return persons.get(name);
	}
}
