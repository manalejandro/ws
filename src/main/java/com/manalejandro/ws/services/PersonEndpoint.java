package com.manalejandro.ws.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.manalejandro.ws.model.GetPersonRequest;
import com.manalejandro.ws.model.GetPersonResponse;
import com.manalejandro.ws.repository.PersonRepository;

@Endpoint
public class PersonEndpoint {
	private static final String NAMESPACE_URI = "http://www.manalejandro.com/person";

	private PersonRepository personRepository;

	@Autowired
	public PersonEndpoint(PersonRepository personRepository) {
		this.personRepository = personRepository;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getPersonRequest")
	@ResponsePayload
	public GetPersonResponse getPerson(@RequestPayload GetPersonRequest request) {
		GetPersonResponse response = new GetPersonResponse();
		response.setPerson(personRepository.findPerson(request.getName()));

		return response;
	}
}
