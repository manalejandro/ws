# How construct `WebServices` with SpringBoot 3.2.1 and Java 17 (modern way)

## Run
```
mvn spring-boot:run -f pom.xml
```

## Example
```
curl -H "content-type: text/xml" -d @request.xml http://localhost:8080/ws
```

## Docker
```
docker build --rm -t ws .
docker run -ti --rm --name ws --network host ws
```
